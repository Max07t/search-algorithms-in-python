def binarysearch(datastructure: list or tuple, tofind):
  sorted_list = sorted(datastructure)
  first, last = (0, len(sorted_list)-1)
  found = False

  while not found and first <= last:
    mid = (first + last) // 2
    if sorted_list[mid] == tofind:
      found = True
    else:
      if tofind > sorted_list[mid]:
        first = mid+1
      else:
        last = mid-1
  return found

# Example: binarysearch([62, 52, 85, 92, 122, 3, 64], 32) -> returns False
# Example: binarysearch([62, 52, 85, 92, 122, 3, 64], 122) -> returns True